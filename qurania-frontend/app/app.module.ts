import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { QuraniaHttpService } from "~/services/quraniaHttpService";
import { QuestionService } from "~/services/QuestionService";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { registerElement } from "nativescript-angular";

registerElement("NumericKeyboard", () => require("nativescript-numeric-keyboard").NumericKeyboardView);

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        QuraniaHttpService,
        QuestionService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule {
}
