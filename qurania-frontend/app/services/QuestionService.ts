import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Global } from "~/modules/configuration/model/Context";
import { GlobalResult } from "~/modules/configuration/model/result";

@Injectable({ providedIn: "root" })
export class QuestionService {
  private _config: any;
  private _user: any;
  constructor(private http: HttpClient) {}

  get config(): Global {
    return this._config;
  }
  set config(value: Global) {
    const items = value.options[0].options;
    items.map(item => {
      const name = item.name;
      const it = item.options[0];
      console.log({
        set: {
          name,
          global: it.global,
          value: it.value
        }
      });
    });
    this._config = value;
  }

  get user(): any {
    return this._user;
  }

  set user(value: any) {
    this._user = value;
  }
}
