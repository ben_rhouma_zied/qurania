import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { GlobalResult } from "../modules/configuration/model/result";

@Injectable()
// Instead of providers array you can use provideIn
// Learn more https://angular.io/guide/providers
// {
//     providedIn: "root"
// }
export class QuraniaHttpService {
  private serverUrl =
  //  "http://169.254.52.129:3000";
  "https://d4dz2s3383.execute-api.us-east-1.amazonaws.com/dev";
  headers = this.createRequestHeader();
  constructor(private http: HttpClient) {}

  getData(path) {
    let headers = this.createRequestHeader();
    return this.http.get(this.serverUrl + path, { headers: headers });
  }

  getUsers() {
    let headers = this.createRequestHeader();
    return this.http.get(this.serverUrl + "/users", { headers: headers });
  }
  getUser(id) {
    let headers = this.createRequestHeader();
    return this.http.get(this.serverUrl + "/users/" + id, { headers: headers });
  }

  getUserResults(id) {
    let headers = this.createRequestHeader();
    return this.http.get(this.serverUrl + "/result/" + id, {
      headers: headers
    });
  }

  saveUsers(name) {
    let headers = this.createRequestHeader();
    return this.http.get(this.serverUrl + "/users", { headers: headers });
  }

  private createRequestHeader() {
    // set headers here e.g.
    let headers = new HttpHeaders({
      AuthKey: "my-key",
      AuthToken: "my-token",
      "Content-Type": "application/json"
    });
    return headers;
  }

  public save(result: GlobalResult) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json"
    });
    this.http.post(this.serverUrl + "/result", JSON.stringify(result), {
      headers
    });
  }

  saveResult(result: GlobalResult) {
    return this.http.post(this.serverUrl + "/result", result, {
      headers: this.createRequestHeader()
    });
  }
}
