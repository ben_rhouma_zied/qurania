import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Global, User } from "~/modules/configuration/model/Context";
import { QuraniaHttpService } from "~/services/quraniaHttpService";
import { RouterExtensions } from "nativescript-angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "Home",
  moduleId: module.id,
  templateUrl: "./user-detail.component.html",
  styleUrls: ["./user-detail.component.css"]
})
export class UserDetailComponent implements OnInit {
  public user: User;
  public results = [];

  constructor(
    private quraniaHttpService: QuraniaHttpService,
    private router: RouterExtensions,
    private route: ActivatedRoute
  ) {}

  async ngOnInit() {
    const params: any = await this.route.params;
    const id = params._value.id;
    this.loadUser(id);
    this.loadResults(id);
  }

  async loadResults(id) {
    try {
      const response: any = await this.quraniaHttpService
        .getUserResults(id)
        .toPromise();
      this.results = response;
    } catch (error) {
      console.log({ error });
    }
  }
  async loadUser(id) {
    try {
      const response: any = await this.quraniaHttpService
        .getUser(id)
        .toPromise();
      const item = response[0];
      if (item) {
        this.user = {
          title: item.first_name + " " + item.last_name,
          id: item._id,
          image: item.image,
          age: item.age
        };
      }
    } catch (error) {
      console.log({ error });
    }
  }

  navigateToConfig() {
    this.router.navigate(["/config", this.user.id]);
  }
}

export interface ResultModel {
  amount: number;
  createdAt: Date;
}
