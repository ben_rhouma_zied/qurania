import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Global, User } from "~/modules/configuration/model/Context";
import { QuraniaHttpService } from "~/services/quraniaHttpService";

@Component({
  selector: "Home",
  moduleId: module.id,
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"]
})
export class UserListComponent implements OnInit {
  data: Global = {
    _id: "5d9aa4c83a3b0e2b48caed9b",
    ConfigType: "percentage",
    amount: 100,
    __v: 0,
    name: "default",
    options: [
      {
        name: "تقييم حفظ",
        global: false,
        actif: true,
        amount: 50,
        _id: "5d9aa4c83a3b0e2b48caed9c",
        options: [
          {
            actif: true,
            name: "فتح",
            global: false,
            _id: "5db0d7ad7321c96f3817743f",
            options: [
              {
                name: "default",
                value: 2,
                global: false,
                _id: "5db0d7cf7321c96f38177443"
              }
            ]
          },
          {
            actif: true,
            name: "تنبيه",
            global: false,
            _id: "5db0d7bf7321c96f38177441",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d7e97321c96f38177445"
              }
            ]
          },
          {
            actif: true,
            name: "تلعثم",
            global: false,
            _id: "5db0d7fb7321c96f38177447",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d8087321c96f38177449"
              }
            ]
          },
          {
            actif: true,
            name: "توقف",
            global: false,
            _id: "5db0d8137321c96f3817744b",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d8f47321c96f38177461"
              }
            ]
          }
        ]
      },
      {
        name: "تقييم تلاوة",
        global: true,
        actif: true,
        amount: 50,
        _id: "5db6257d5b773922e6e1d317",
        options: [
          {
            actif: true,
            name: "المدود",
            global: false,
            _id: "5db0d8257321c96f3817744d",
            options: [
              {
                name: "default",
                value: 2,
                global: false,
                _id: "5db0d8327321c96f3817744f"
              }
            ]
          },
          {
            actif: true,
            name: "النون والميم",
            global: false,
            _id: "5db0d8437321c96f38177451",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d84d7321c96f38177453"
              },
              {
                name: "noun",
                value: 1,
                global: false,
                _id: "5db213ead6664d2268f21f76"
              }
            ]
          },
          {
            actif: true,
            name: "المخارج والصفات",
            global: false,
            _id: "5db0d85a7321c96f38177455",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d8637321c96f38177457"
              }
            ]
          },
          {
            actif: true,
            name: "الوقف والإبتداء",
            global: false,
            _id: "5db0d8707321c96f38177459",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d8797321c96f3817745b"
              }
            ]
          },
          {
            actif: true,
            name: "حسن الصوت",
            global: true,
            _id: "5db0d8867321c96f3817745d",
            options: [
              {
                name: "default",
                value: 1,
                global: false,
                _id: "5db0d8947321c96f3817745f"
              }
            ]
          }
        ]
      }
    ]
  };
  isBusy = true;
  public users: User[] = [];

  constructor(
    private quraniaHttpService: QuraniaHttpService,
    private router: RouterExtensions
  ) {}

  ngOnInit(): void {
    this.loadUsers();
  }

  async loadUsers() {
    try {
      this.quraniaHttpService.getUsers().subscribe((result: any[]) => {
        this.users = result.map(
          item =>
            new User({
              id: item._id,
              title: item.first_name + " " + item.last_name,
              age: item.age,
              image: item.image
            })
        );
      });
    } catch (error) {
      console.log({ error });
    }
  }
  navigateToUser(id) {
    this.router.navigate(["home/users", id]);
  }
}
