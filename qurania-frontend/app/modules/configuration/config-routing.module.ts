import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ConfigComponent } from "./config/config.component";
import { ConfigQuestionComponent } from "~/modules/configuration/questions/config.question.component";
import { ConfigRecapComponent } from "~/modules/configuration/recap/config.recap.component";
import { ConfigResultComponent } from "~/modules/configuration/result/config.result.component";

const routes: Routes = [
  { path: ":id", component: ConfigComponent },
  { path: "rate/:id", component: ConfigQuestionComponent },
  { path: "recap/:id", component: ConfigRecapComponent },
  { path: "result/:id", component: ConfigResultComponent }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ConfigRoutingModule {}
