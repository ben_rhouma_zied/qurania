import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { CardView } from "../../../@nstudio/nativescript-cardview";
import { QuraniaHttpService } from "~/services/quraniaHttpService";
import { RouterExtensions } from "nativescript-angular";
import { QuestionService } from "~/services/QuestionService";
import { SwipeDirection } from "ui/gestures";
import { Global, GlobalComp } from "~/modules/configuration/model/Context";
import { ActivatedRoute } from "@angular/router";

registerElement("CardView", () => CardView);

@Component({
  selector: "ConfigQuestion",
  moduleId: module.id,
  templateUrl: "./config.question.component.html",
  styleUrls: ["./config.question.component.css"]
})
export class ConfigQuestionComponent implements OnInit {
  config: Global;
  dialogOpen = false;
  public id = "xyz";

  constructor(
    private myService: QuraniaHttpService,
    private questionService: QuestionService,
    private route: ActivatedRoute,
    private router: RouterExtensions
  ) {}

  async ngOnInit() {
    this.config = this.questionService.config;
    this.config.options.forEach(value => {
      value.options.forEach(value1 => {
        value1.nb = 0;
        value1.options.forEach(value2 => {
          value2.nb = 0;
        });
      });
    });
    //console.log({ savedConfig: this.config });
    const params: any = await this.route.params;
    if (params._value.id) {
      this.id = params._value.id;
    }
  }

  closeDialog() {
    this.dialogOpen = false;
  }
  submit() {
   
    let global = false;
    this.config.options.forEach(value => {
      
      value.options.forEach(value1 => {
        
        value1.options.forEach(value2 => {
          if(value2.global){
              global=true;
          }
        });

      });

    });
    if(global){
      this.router.navigate(["config/recap", this.id]);
    }else{
      this.router.navigate(["config/result", this.id]);
    }
  }

  onMinus(item) {
    if (item.nb != 0) {
      item.nb--;
    }
  }

  onAdd(item) {
    item.nb++;
  }
}
