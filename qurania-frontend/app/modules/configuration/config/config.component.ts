import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { CardView } from "../../../@nstudio/nativescript-cardview";
import { QuraniaHttpService } from "~/services/quraniaHttpService";
import { RouterExtensions } from "nativescript-angular";
import { LoadingIndicator } from "@nstudio/nativescript-loading-indicator";
import { Page } from "ui/page";
import { LayoutBase } from "ui/layouts/layout-base";
import { QuestionService } from "~/services/QuestionService";
import { ActivatedRoute } from "@angular/router";

registerElement("CardView", () => CardView);

@Component({
  selector: "Config",
  moduleId: module.id,
  templateUrl: "./config.component.html",
  styleUrls: ["./config.component.css"]
})
export class ConfigComponent implements OnInit {
  public config: any;

  public host: string;
  public userAgent: string;
  public origin: string;
  public url: string;
  public id: string;
  private indicator: LoadingIndicator;

  constructor(
    private page: Page,
    private myService: QuraniaHttpService,
    private questionService: QuestionService,
    private router: RouterExtensions,
    private route: ActivatedRoute
  ) {}

  loadConfiguration() {
    this.myService.getData("/configuration/default").subscribe(
      (result: any) => {
        this.config = result;
        this.indicator.hide();
      },
      error => {
        console.log(error);
      }
    );
  }

  submit() {
    this.questionService.config = this.config;
    this.router.navigate(["config/rate", this.id]);
  }

  private onGetDataSuccess(res) {
    this.host = res.headers.Host;
    this.userAgent = res.headers["User-Agent"];
    this.origin = res.origin;
    this.url = res.url;
  }

  async ngOnInit() {
    this.loadConfiguration();
    const params: any = await this.route.params;
    if (params._value.id) {
      this.id = params._value.id;
    }
  }

  back() {
    this.router.navigate([""]);
  }

  openHide(item) {
    console.log({ name: item.name, actif: item.actif });
    let q: LayoutBase = this.page.getViewById(item._id);
    if (!item.actif) {
      q.height = "auto";
    } else {
      q.height = 0;
    }
  }
  hideOpen(item) {
    console.log({ name: item.name, actif: item.actif });
    let q: LayoutBase = this.page.getViewById(item._id);
    if (!item.actif) {
      q.height = "auto";
    } else {
      q.height = 0;
    }
  }
}
