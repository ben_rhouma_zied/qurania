import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { CardView } from "../../../@nstudio/nativescript-cardview";
import { QuraniaHttpService } from "~/services/quraniaHttpService";
import { RouterExtensions } from "nativescript-angular";
import { QuestionService } from "~/services/QuestionService";
import { SwipeDirection } from "ui/gestures";
import { Bloc, Detail, GlobalResult } from "../model/result";
import {
  Global,
  GlobalComp,
  ItemComp
} from "~/modules/configuration/model/Context";
import { ActivatedRoute } from "@angular/router";

registerElement("CardView", () => CardView);

@Component({
  selector: "ConfigResult",
  moduleId: module.id,
  templateUrl: "./config.result.component.html",
  styleUrls: ["./config.result.component.css"]
})
export class ConfigResultComponent implements OnInit {
  config: Global;
  result: GlobalResult;

  constructor(
    private myService: QuraniaHttpService,
    private questionService: QuestionService,
    private route: RouterExtensions,
    private router: ActivatedRoute
  ) {
    this.config = questionService.config;
  }
  getAmount(item: ItemComp): number {
    return item.options.find(elem => elem.name == "default").value;
  }

  async ngOnInit() {
    const params: any = await this.router.params;

    this.result = {
      amount: this.config.amount,
      name: this.config.name,
      config: this.config,
      userId: params._value.id
    };

    this.config.options.map((config: GlobalComp) => {
      let bloc: Bloc = {
        name: config.name
      };
      config.options.map(item => {
        if (item.global == true) {
          let plus: Detail = {
            name: item.name,
            amount: item.nb * this.getAmount(item)
          };
          this.result.amount += item.nb * this.getAmount(item);
          if (!bloc.plus) {
            bloc.plus = Array();
          }
          bloc.plus.push(plus);
        } else {
          let minus: Detail = {
            name: item.name,
            amount: item.nb * this.getAmount(item)
          };
          this.result.amount -= item.nb * this.getAmount(item);

          if (!bloc.minus) {
            bloc.minus = Array();
          }
          bloc.minus.push(minus);
        }
      });

      if (!this.result.details) {
        this.result.details = Array();
      }
      this.result.details.push(bloc);
    });
    console.log({ details: this.result.details });
  }

  getRows(array) {
    let rows = "";
    if (!array) {
      return "0";
    }
    for (let i = 0; i < array.length; i++) {
      rows += ",50";
    }
    return rows;
  }

  async saveResult() {
    try {
      const result: any = await this.myService
        .saveResult(this.result)
        .toPromise();
      this.route.navigate(["home/users", this.result.userId]);
    } catch (error) {
      console.log({ error });
    }
  }
}
