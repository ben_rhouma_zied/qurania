import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { CardView } from "../../../@nstudio/nativescript-cardview";
import { QuraniaHttpService } from "~/services/quraniaHttpService";
import { RouterExtensions } from "nativescript-angular";
import { QuestionService } from "~/services/QuestionService";
import { SwipeDirection } from "ui/gestures";
import { Global, GlobalComp } from "~/modules/configuration/model/Context";
import { ActivatedRoute } from "@angular/router";

registerElement("CardView", () => CardView);

@Component({
  selector: "ConfigRecap",
  moduleId: module.id,
  templateUrl: "./config.recap.component.html",
  styleUrls: ["./config.recap.component.css"]
})
export class ConfigRecapComponent implements OnInit {
  config: Global;
  public host: string;
  public userAgent: string;
  public origin: string;
  public url: string;
  public id = "myid";
  constructor(
    private myService: QuraniaHttpService,
    private questionService: QuestionService,
    private router: ActivatedRoute,
    private route: RouterExtensions
  ) {}

  async ngOnInit() {
    this.config = this.questionService.config;
    console.log({ recap: this.config });
    const params: any = await this.router.params;
    this.id = params._value.id;
  }
  ret() {
    this.route.navigate(["config/result/", this.id]);
  }
}
