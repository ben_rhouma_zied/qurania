interface GlobalComp {
  name: string;
  global: boolean;
  actif: boolean;
  amount: number;
  _id: string;
  options: ItemComp[];
}

interface ItemComp {
  actif: boolean;
  name: string;
  global: boolean;
  _id: string;
  nb?: number;
  options: ItemDetails[];
}

interface ItemDetails {
  name: string;
  value: number;
  global: boolean;
  _id: string;
  nb?: number;
}

interface Global {
  user?: User;
  _id: string;
  ConfigType: string;
  amount: number;
  __v: number;
  name: string;
  options: Array<GlobalComp>;
}

class User {
  constructor(person: User) {
    Object.assign(this, person);
  }

  id?: string;
  description?: string;
  title: string;
  image?: string;
  age?: string;
}

export { Global, GlobalComp, ItemDetails, ItemComp, User };
