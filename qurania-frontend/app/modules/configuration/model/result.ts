import { Global } from "../model/Context";
interface GlobalResult {
  amount: number;
  name: string;
  userId?: string;
  details?: Array<Bloc>;
  config: Global;
}

interface Bloc {
  name: string;
  minus?: Array<Detail>;
  plus?: Array<Detail>;
}

interface Detail {
  name: string;
  amount: number;
}

export { GlobalResult, Bloc, Detail };
