import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUICalendarModule } from "nativescript-ui-calendar/angular";
import { NativeScriptUIChartModule } from "nativescript-ui-chart/angular";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { NativeScriptUIAutoCompleteTextViewModule } from "nativescript-ui-autocomplete/angular";
import { NativeScriptUIGaugeModule } from "nativescript-ui-gauge/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { ConfigRoutingModule } from "./config-routing.module";
import { ConfigComponent } from "./config/config.component";
import { ConfigQuestionComponent } from "~/modules/configuration/questions/config.question.component";
import { ConfigRecapComponent } from "~/modules/configuration/recap/config.recap.component";
import { ConfigResultComponent } from "~/modules/configuration/result/config.result.component";

@NgModule({
  imports: [
    NativeScriptUISideDrawerModule,
    NativeScriptUIListViewModule,
    NativeScriptUICalendarModule,
    NativeScriptUIChartModule,
    NativeScriptUIDataFormModule,
    NativeScriptUIAutoCompleteTextViewModule,
    NativeScriptUIGaugeModule,
    NativeScriptCommonModule,
    ConfigRoutingModule,
    NativeScriptFormsModule
  ],
  declarations: [
    ConfigComponent,
    ConfigQuestionComponent,
    ConfigRecapComponent,
    ConfigResultComponent
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ConfigModule {}
