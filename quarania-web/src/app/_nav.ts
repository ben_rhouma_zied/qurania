interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
  role?: string;
}

export const navItems: NavData[] = [
  {
    title: true,
    name: 'Menu',
    role: 'user'
  },
  {
    name: 'Home',
    url: '/dashboard/home',
    icon: 'icon-home',
    role: 'user'
  },
  {
    name: 'المستخدمين',
    url: '/dashboard/users',
    icon: 'icon-people',
    role: 'user',
    children: [
      {
        name: 'أضف مستخدم',
        url: '/dashboard/users/create',
        icon: 'icon-plus',
        role: 'admin',
      },
      {
        name: 'قائمة المستخدمين',
        url: '/dashboard/users/list',
        icon: 'icon-user',
        role: 'user'
      }
    ],
  },
  {
    name: 'الجمعيات',
    url: '/dashboard/jamia',
    icon: 'icon-people',
    role: 'super',
    children: [
      {
        name: 'قائمة الجمعيات',
        url: '/dashboard/jamia/list',
        icon: 'icon-user',
        role: 'super'
      }
    ],
  },
  {
    name: 'خيارات الأسئلة',
    url: '/dashboard/config',
    icon: 'icon-people',
    role: 'admin',
    children: [
      {
        name: 'قائمة الخيارات',
        url: '/dashboard/config/list',
        icon: 'icon-user',
        role: 'admin'
      },
      {
        name: 'أضف',
        url: '/dashboard/config/edit',
        icon: 'icon-plus',
        role: 'admin'
      }
    ],
  }
];

