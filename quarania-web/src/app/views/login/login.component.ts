import {Component} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';

@Component({selector: 'app-dashboard', templateUrl: 'login.component.html'})
export class LoginComponent {
    loginForm : FormGroup;
    errorMessage : string = '';
    loginImage : any = require('../../../assets/img/brand/logo.png');

    constructor(public authService : AuthService, private router : Router, private fb : FormBuilder) {
        this.createForm();
    }

    createForm() {
        this.loginForm = this.fb.group({
            email: [
                '', Validators.required
            ],
            password: ['', Validators.required]
        });
    }

    async tryLogin(value) {
        this.errorMessage = '';
        this.authService.loginUser(value.email, value.password).subscribe(data => {
            if (!! data.token) {
                this.authService.setToken(data.token);
                this.router.navigate(['/dashboard/home']);
            } else {
                this.errorMessage = 'invalid credentials';
            }
        }, err => {
            console.log(err);
            if (err.status === 0) {
                this.errorMessage = 'connection refused';
            }
            if (err.status === 200) {
                this.errorMessage = 'invalid credentials';
            }
        });
        /*
        try {
            const data = await this.authService.loginUser(value.email, value.password).toPromise();
            if (!! data.token) {
                this.authService.setToken(data.token);
                this.router.navigate(['/dashboard/home']);
            } else {
                this.errorMessage = 'invalid credentials';
            }
        } catch (err) {
            this.errorMessage = err;
            if (err.error) {
                this.errorMessage = err.error.text;
            }
        }*/

    }

}
