import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: 'home',
    component: DashboardComponent,
    data: {
      title: 'Dashboard',
    },
  },
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then(mod => mod.UserModule),
  },
  {
    path: 'jamia',
    loadChildren: () => import('./jamia/jamia.module').then(mod => mod.JamiaModule),
  },
  {
    path: 'config',
    loadChildren: () => import('./config/config.module').then(mod => mod.ConfigModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
