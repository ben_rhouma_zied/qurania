import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { UserService } from '../../services/user.service';
import { JamiaService } from '../../services/jamia.service';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { ProjectService } from 'app/services/projects.service';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { StatsService } from 'app/services/stats.service';
import { DxButtonModule } from 'devextreme-angular';
import { ConfigService } from '../../services/config.service';

@NgModule({
  imports: [
    FormsModule,
    DashboardRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    TabsModule,
    BsDropdownModule,
    DxButtonModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    DataTablesModule,
    ChartsModule,
  ],
  declarations: [DashboardComponent],
  providers: [UserService, JamiaService, ProjectService, ConfigService, StatsService],
})
export class DashboardModule {}
