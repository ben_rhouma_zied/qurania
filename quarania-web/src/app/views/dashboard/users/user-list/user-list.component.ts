import { Component, OnInit} from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../auth/auth.service';

@Component({
  selector: 'app-user-list-gc',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  attachUsersToCurrentUser = false;
  dataSource: any = {};
  private readonly service: UserService;
  private readonly route: Router;
  filterValue: Array<any>;
  popupPosition: any;

  myModal: any;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;

  private fb: FormBuilder;
  uploadForm: FormGroup;
  fileData: File = null;
  private auth: AuthService;
  role = 'user';
  users;
  loadingVisible: boolean;

  constructor(service: UserService
    , route: Router
    , auth: AuthService
  ) {
    this.route = route;
    this.service = service;
    this.auth = auth;
    this.role = this.auth.getRole();
    this.attachUsersToCurrentUser = (this.role === 'admin');
    this.popupPosition = {of: window, at: 'top', my: 'top', offset: {y: 10}};
    this.filterValue = [];
    this.loadingVisible = true;

  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    // Show preview
    const mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    };
  }

  async onSubmit(modal) {
    const formData = new FormData();
    formData.append('file', this.fileData);
    formData.append('mode', String(this.attachUsersToCurrentUser));
    await this.service.uploadFile(formData);
    modal.hide();
    this.loadUsers();
  }

  async deleteUser(id) {
    const resp = await this.service.deleteUser(id);
    this.users.docs = this.users.docs.filter(e => e._id !== id);
    this.users.totalDocs = this.users.docs.length;
    this.initUsers();
  }

  createForm() {

    this.uploadForm = this.fb.group({
      file: ['', Validators.required]
    });
  }

  onInitialized(e) {
    e.component.columnOption('SaleAmount', {
      editorOptions: {
        format: 'currency',
        showClearButton: true
      }
    });
  }

  async ngOnInit() {
    this.loadUsers();
  }

  async loadUsers() {
    const userService = this.service;
    const authService = this.auth;
    const route = this.route;
    try {
      this.users = await userService.getUsers(authService.getLoggedUserId(), authService.getRole());
    } catch (e) {
      if (e.status === 401) {
        authService.removeToken();
        return route.navigate(['/login']);
      }
    }
    this.initUsers();
  }

  initUsers() {
    const users = this.users;
    const that = this;
    this.dataSource = new CustomStore({
      key: '_id',
      load: async function(loadOptions: any) {
        let params: HttpParams = new HttpParams();
        [
          'skip',
          'take',
          'requireTotalCount',
          'requireGroupCount',
          'sort',
          'filter',
          'totalSummary',
          'group',
          'groupSummary'
        ].forEach(function(i) {
          if (i in loadOptions && loadOptions[i] !== undefined && loadOptions[i] !== null && loadOptions[i] !== '') {
            params = params.set(i, JSON.stringify(loadOptions[i]));
          }
        });
        that.loadingVisible = false;
        return {
          data: users.docs,
          totalCount: users.totalDocs,
          summary: null,
          groupCount: null
        };
      }
    });
  }
}
