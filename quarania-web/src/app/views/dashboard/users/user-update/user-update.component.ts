import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { UserModel } from '../../../../models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../auth/auth.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  user: UserModel;
  passwordForm: FormGroup;
  userForm: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
  errorMessageUser: string = '';
  successMessageUser: string = '';
  options;
  private role: string;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private userService: UserService,
    auth: AuthService
  ) {
    this.createForm();
    this.role = auth.getRole();
  }

  createForm() {
    this.passwordForm = this.fb.group({
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    });
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      entity: ['', Validators.required],
      type: [''],
      email: [null, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
        Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)])]
    });
  }

  async ngOnInit() {
    const id = this.route.snapshot.params['id'];
    if (id) {
      await this.loadUser(id);
      this.options = await this.buildModel();
      console.log(this.options);
    }
  }

  async buildModel() {
    const opts: Array<any> = <Array<any>>await this.userService.getEntities();
    return opts.filter(e => e.parent === null).map(e => {
      return {
        'id': e._id,
        'label': e.name,
        'choices':
          opts.filter(child => child.parent === e._id).map(child => {
            return {
              'value': child._id,
              'label': child.name
            };
          })
      };
    }).map(e => {
      if (e.choices && e.choices.length > 0) {
        return e;
      } else {
        return {
          'id': e.id,
          'label': e.label,
        };
      }
    });
  }

  get email() {
    return this.userForm.get('email');
  }

  get firstName() {
    return this.userForm.get('firstName');
  }

  get lastName() {
    return this.userForm.get('lastName');
  }

  get entity() {
    return this.userForm.get('entity');
  }

  get type() {
    return this.userForm.get('type');
  }

  async loadUser(id) {
    try {
      const response = await this.userService.getUser(id).toPromise();
      console.log({response});

      if (response) {
        this.user = response[0];
        console.log(this.user);
        this.loadUserInfo();
      }
    } catch (e) {
      console.log({e});
    }

  }

  async loadUserInfo() {
    this.userForm.get('firstName').setValue(this.user.first_name);
    this.userForm.get('lastName').setValue(this.user.last_name);
    this.userForm.get('email').setValue(this.user.email);
    this.userForm.get('entity').setValue(this.user.entity);
    this.userForm.get('type').setValue(this.user.type);
  }

  async changePassword(value) {
    const pwd = value.password;
    const rpd = value.repeatPassword;
    this.errorMessage = '';
    this.successMessage = '';
    setTimeout(() => {
      if (pwd.length < 6) {
        this.errorMessage = 'Password must be greater than 8 characters';
        return false;
      }
      if (rpd !== pwd) {
        this.errorMessage = 'Passwords have to be matched';
        return false;
      }
      const id = this.user._id;
      this.userService.changePassword(id, pwd).subscribe(
        data => {
          this.successMessage = 'updated password successfully';
        },
        err => {
          this.errorMessage = 'error on updating password';
        }
      );
    }, 300);
  }

  async updateUser() {
    if (!this.userForm.valid) {
      return false;
    }
    this.errorMessageUser = '';
    this.successMessageUser = '';
    this.user.first_name = this.userForm.get('firstName').value;
    this.user.last_name = this.userForm.get('lastName').value;
    this.user.email = this.userForm.get('email').value;
    this.user.entity = this.userForm.get('entity').value;
    this.user.type = this.userForm.get('type').value;
    this.userService.updateUser(this.user).subscribe(
      data => {
        this.successMessageUser = 'updated successfully';
      },
      err => {
        this.errorMessageUser = 'error on updating';
      }
    );
  }

}
