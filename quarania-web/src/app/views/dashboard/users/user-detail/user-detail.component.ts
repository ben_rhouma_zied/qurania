import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { UserModel } from '../../../../models/user.model';
import { LogModel } from '../../../../models/logs.model';
import { ProjectModel, ProjectQuery } from '../../../../models/project.model';
import { ProjectService } from '../../../../services/projects.service';
import { environment } from '../../../../../environments/environment';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';
import { AuthService } from "../../../../auth/auth.service";

@Component({ selector: 'app-user-detail', templateUrl: './user-detail.component.html', styleUrls: ['./user-detail.component.scss'] })
export class UserDetailComponent implements OnInit {
  dataSource: CustomStore;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private userService: UserService,
  ) {
    this.role = this.auth.getRole();
  }
  user: UserModel;
  role;
  id: string = '0';
  updateSuccessMessage = '';
  updateErrorMessage = '';

  acts = [
    'Logged in',
    'Logged out',
    'Reset Password',
    'Accept Bid',
    'Accept User connection',
    'Decline Bid',
    'Decline User connection',
    'Update Profile',
    'Create Project',
    'Update Project',
    'Delete Project',
    'Create Bid',
    'Update Bid',
    'Delete Bid',
  ];
  async ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    this.user = <UserModel> await this.loadUser(this.id );
    const that = this;
    this.dataSource = new CustomStore({
      key: '_id',
      load: async function(loadOptions: any) {
        let params: HttpParams = new HttpParams();
        [
          'skip',
          'take',
          'requireTotalCount',
          'requireGroupCount',
          'sort',
          'filter',
          'totalSummary',
          'group',
          'groupSummary'
        ].forEach(function(i) {
          if (i in loadOptions && loadOptions[i] !== undefined && loadOptions[i] !== null && loadOptions[i] !== '') {
            params = params.set(i, JSON.stringify(loadOptions[i]));
          }
        });

        return {
          data: that.user.results,
          totalCount: that.user.results.length,
          summary: null,
          groupCount: null
        };
      }
    });
  }

  async loadUser(id) {
      const response: any = await this.userService.getUser(id).toPromise();
      return response[0];
  }

  openExternalLink(url) {
    window.open(
      url,
      '_blank' // <- This is what makes it open in a new window.
    );
  }



  resetMessages() {
    this.updateErrorMessage = '';
    this.updateSuccessMessage = '';
  }

}
