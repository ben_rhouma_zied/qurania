import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { UserModel } from '../../../../models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { log } from 'util';
@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {
  user: UserModel;
  userForm: FormGroup;
  errorMessageUser = '';
  successMessageUser = '';
  options = this.userService.getEntities();
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private userService: UserService
  ) {
    this.createForm();
  }
  createForm() {

    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      choice: ['', Validators.required],
      email: [null, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
        Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)])],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    },
      {
        validator: MustMatch('password', 'confirmPassword')
      });
  }
  ngOnInit() {
  }

  onChange(value) {
  }

  async createUser() {
    if (!this.userForm.valid) {
      console.log(this.userForm);
      return false;
    }
    this.errorMessageUser = '';
    this.successMessageUser = '';
    this.user = {
      type: 'user',
      email: this.userForm.get('email').value,
      first_name: this.userForm.get('firstName').value,
      last_name: this.userForm.get('lastName').value,
      entity: this.userForm.get('entity').value,
      password: this.userForm.get('password').value
    };
    const password = this.userForm.get('password').value;
    const confirmPassword = this.userForm.get('confirmPassword').value;
    this.userService.createUser(this.user, password, confirmPassword).subscribe(
      data => {
        this.successMessageUser = 'Created successfully';
      },
      err => {
        this.errorMessageUser = 'Error on creating';
      }
    );
  }

  get email() {
    return this.userForm.get('email');
  }
  get firstName() {
    return this.userForm.get('firstName');
  }
  get lastName() {
    return this.userForm.get('lastName');
  }
  get password() {
    return this.userForm.get('password');
  }
  get confirmPassword() {
    return this.userForm.get('confirmPassword');
  }
  get choice() {
    return this.userForm.get('choice');
  }

}
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}
