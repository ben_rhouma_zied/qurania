import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { DataTablesModule } from 'angular-datatables';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NbChoicesModule } from 'nb-choices';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../../../shared/shared.module';
import { environment } from '../../../../environments/environment';
import { ModalModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { DxButtonModule, DxCheckBoxModule, DxDataGridModule, DxLoadPanelModule } from 'devextreme-angular';
import {DxoTextModule} from 'devextreme-angular/ui/nested';

const STRIPE_PK = environment.STRIPE_PK;
const routes: Routes = [
  {
    path: 'create',
    component: UserCreateComponent,
    data: {
      title: 'Create User',
    },
  },
  {
    path: 'list',
    component: UserListComponent,
    data: {
      title: 'General Contractor List',
    },
  },
  {
    path: 'detail/:id',
    component: UserDetailComponent,
    data: {
      title: 'User Details',
    },
  },
  {
    path: 'update/:id',
    component: UserUpdateComponent,
    data: {
      title: 'User Update',
    },
  },
];
@NgModule({
    imports: [
        CommonModule,
        DataTablesModule,
        AlertModule.forRoot(),
        TabsModule,
        NbChoicesModule,
        MatIconModule,
        MatTabsModule,
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        ModalModule,
        HttpClientModule,
        DxButtonModule,
        DxDataGridModule,
        DxoTextModule,
        FormsModule,
        DxCheckBoxModule,
        DxLoadPanelModule
    ],
  declarations: [
    UserDetailComponent,
    UserUpdateComponent,
    UserListComponent,
    UserCreateComponent,
  ],
})
export class UserModule {}
