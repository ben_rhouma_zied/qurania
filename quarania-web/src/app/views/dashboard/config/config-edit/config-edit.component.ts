import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from '../../../../services/config.service';

@Component({
  templateUrl: './config-edit.component.html',
  styleUrls: ['./config-edit.component.scss']
})
export class ConfigEditComponent implements OnInit {
  public config: Config;
  private configService: ConfigService;

  constructor(configService: ConfigService, private route: Router) {
    this.configService = configService;
  }

  async ngOnInit() {

    await this.getConfig();
  }

  async getConfig() {
    if (!this.configService.config) {
      this.config = <Config> JSON.parse(JSON.stringify(await this.configService.getConfig()));
      this.config.name = 'برنامج_' + new Date().toISOString();
      delete this.config._id;
    } else {
      this.config = this.configService.config;
    }
  }

  async saveConfig() {
    console.log(this.config);
    await this.configService.saveConfig(this.config);
    this.configService.config = null;
    await this.route.navigate(['/dashboard/config/list']);
  }
}
