import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfigService } from '../../../../services/config.service';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';

@Component({
  templateUrl: './config-list.component.html',
  styleUrls: ['./config-list.component.scss']
})
export class ConfigListComponent implements OnInit {
  public config: any;
  private configService: ConfigService;

  constructor(configService: ConfigService, private route: Router) {
    this.configService = configService;
  }

  configs;
  auth;
  dataSource;

  async ngOnInit() {
    this.loadConfigs();
  }

  async loadConfigs() {
    const authService = this.auth;
    try {
      this.configs = await this.configService.getConfigs();
    } catch (e) {
      if (e.status === 401) {
        authService.removeToken();
        return this.route.navigate(['/login']);
      }
    }
    this.initConfigs();
  }

  async deleteConfig(id) {
    await this.configService.deleteConfig(id);
    const that = this;
    this.configs = this.configs.filter(e => e._id !== id);
    this.dataSource = new CustomStore({
      key: '_id',
      load: async function(loadOptions: any) {
        let params: HttpParams = new HttpParams();
        [
          'skip',
          'take',
          'requireTotalCount',
          'requireGroupCount',
          'sort',
          'filter',
          'totalSummary',
          'group',
          'groupSummary'
        ].forEach(function(i) {
          if (i in loadOptions && loadOptions[i] !== undefined && loadOptions[i] !== null && loadOptions[i] !== '') {
            params = params.set(i, JSON.stringify(loadOptions[i]));
          }
        });

        return {
          data: that.configs,
          totalCount: that.configs.length,
          summary: null,
          groupCount: null
        };
      }
    });
  }

  initConfigs() {
    const configs = this.configs;
    this.dataSource = new CustomStore({
      key: '_id',
      load: async function(loadOptions: any) {
        let params: HttpParams = new HttpParams();
        [
          'skip',
          'take',
          'requireTotalCount',
          'requireGroupCount',
          'sort',
          'filter',
          'totalSummary',
          'group',
          'groupSummary'
        ].forEach(function(i) {
          if (i in loadOptions && loadOptions[i] !== undefined && loadOptions[i] !== null && loadOptions[i] !== '') {
            params = params.set(i, JSON.stringify(loadOptions[i]));
          }
        });

        return {
          data: configs,
          totalCount: configs.length,
          summary: null,
          groupCount: null
        };
      }
    });
  }

  async edit(id) {
    // console.log({id});
      this.configService.config = await this.configService.getConfig(id);
      this.route.navigate(['/dashboard/config/edit']);
  }
}
