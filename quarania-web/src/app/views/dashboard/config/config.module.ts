import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ConfigListComponent } from './config-list-impl/config-list.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../../../shared/shared.module';
import { environment } from '../../../../environments/environment';
import {
  DxAccordionModule, DxBoxModule,
  DxButtonModule, DxCheckBoxModule,
  DxDataGridModule, DxFormModule,
  DxNumberBoxModule,
  DxPopupModule, DxSliderModule, DxSwitchModule, DxTagBoxModule, DxTextBoxModule,
  DxTreeListModule
} from 'devextreme-angular';
import {NbChoicesModule} from 'nb-choices';
import { ConfigEditComponent } from './config-edit/config-edit.component';

const routes: Routes = [
  {
    path: 'list',
    component: ConfigListComponent,
    data: {
      title: 'Config List',
    }
  },
  {
    path: 'edit',
    component: ConfigEditComponent,
    data: {
      title: 'Config edit',
    }
  }
];
@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    AlertModule.forRoot(),
    TabsModule,
    MatIconModule,
    MatTabsModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DxTreeListModule,
    NbChoicesModule,
    DxDataGridModule,
    DxButtonModule,
    DxPopupModule,
    DxNumberBoxModule,
    DxSwitchModule,
    DxAccordionModule,
    DxCheckBoxModule,
    DxSliderModule,
    DxTagBoxModule,
    FormsModule,
    DxFormModule,
    DxBoxModule,
    DxTextBoxModule,
  ],
  declarations: [
    ConfigListComponent,
    ConfigEditComponent
  ],
})
export class ConfigModule {}
