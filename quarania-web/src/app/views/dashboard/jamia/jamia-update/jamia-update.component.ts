import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JamiaService} from '../../../../services/jamia.service';
import {JamiaModel} from '../../../../models/jamia.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user.service';
import CustomStore from 'devextreme/data/custom_store';
import {HttpParams} from '@angular/common/http';
import {DxDataGridComponent} from 'devextreme-angular';

@Component({
  selector: 'app-jamia-update',
  templateUrl: './jamia-update.component.html',
  styleUrls: ['./jamia-update.component.scss']
})
export class JamiaUpdateComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataGrid: DxDataGridComponent;
  jamia: JamiaModel;
  form: FormGroup;
  popupVisible = false;
  errorMessage: string = '';
  successMessage: string = '';
  dataSource: any = {};
  options: any;
  private userService: UserService;
  private jamiaInfo;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private jamiaService: JamiaService,
    userService: UserService
  ) {
    this.userService = userService;

  }

  async ngOnInit() {
    const id = this.route.snapshot.params['id'];
    if (id) {
      this.jamiaInfo = await this.jamiaService.getJamia(id).toPromise();
      this.prepareDataSet();
      await this.loadJamia();
    }
  }

  save() {
    const id = this.route.snapshot.params['id'];
    const ids = [];
    this.dataGrid.instance.getSelectedRowsData().then((rowData) => {
      for (let i = 0; i < rowData.length; i++) {
        console.log(rowData[i]._id);
          ids.push(rowData[i]._id);
      }
      this.updateJamia(id, ids);
    });
  }

  private prepareDataSet() {

    this.jamiaInfo.users.forEach(user => {
      this.jamiaInfo.organisation.users.forEach(orgUser => {
        if (user._id === orgUser._id) {
          user.selected = true;
        }
      });

      if ( user.selected == null ) {
        user.selected = false;
      }

    });
  }



  async loadJamia() {
    this.dataSource = new CustomStore({
      key: '_id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();
        [
          'skip',
          'take',
          'requireTotalCount',
          'requireGroupCount',
          'sort',
          'filter',
          'totalSummary',
          'group',
          'groupSummary'
        ].forEach(function (i) {
          if (i in loadOptions && loadOptions[i] !== undefined && loadOptions[i] !== null && loadOptions[i] !== '') {
            params = params.set(i, JSON.stringify(loadOptions[i]));
          }
        });

        return {
          data: this.jamiaInfo.users,
          totalCount: this.jamiaInfo.users.length,
          summary: null,
          groupCount: null
        };
      }
    });
  }


  async updateJamia(id, ids) {
    this.errorMessage = '';
    this.successMessage = '';
    this.jamiaService.updateJamia(id, ids).subscribe(
      data => {
        this.successMessage = 'updated successfully';
      },
      err => {
        this.errorMessage = 'error on updating';
      }
    );
  }

}
