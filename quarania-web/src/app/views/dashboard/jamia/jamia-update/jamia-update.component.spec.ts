import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JamiaUpdateComponent } from './jamia-update.component';

describe('JamiaUpdateComponent', () => {
  let component: JamiaUpdateComponent;
  let fixture: ComponentFixture<JamiaUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JamiaUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JamiaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
