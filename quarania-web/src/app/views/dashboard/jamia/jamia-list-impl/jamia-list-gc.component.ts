import { Component, OnInit, ViewChild } from '@angular/core';
import { JamiaService } from '../../../../services/jamia.service';
import { ActivatedRoute } from '@angular/router';
import * as http from 'http';

@Component({
  selector: 'app-jamia-list-gc',
  templateUrl: './jamia-list-gc.component.html',
  styleUrls: ['./jamia-list-gc.component.scss']
})
export class JamiaListGcComponent implements OnInit {
  page: number = 1;
  limit = 10;
  private jamiaService: JamiaService;
  organisations: any;
  constructor(jamiaService: JamiaService, private route: ActivatedRoute) {
    this.jamiaService = jamiaService;
  }

  ngOnInit() {
    this.getOrganisations();
  }

  async getOrganisations() {
    this.organisations = await this.jamiaService.getJamias();
  }

}
