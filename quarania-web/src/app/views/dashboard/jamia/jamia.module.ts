import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { JamiaListGcComponent } from './jamia-list-impl/jamia-list-gc.component';
import { JamiaUpdateComponent } from './jamia-update/jamia-update.component';
import { DataTablesModule } from 'angular-datatables';
import { ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { SharedModule } from '../../../shared/shared.module';
import { environment } from '../../../../environments/environment';
import {DxButtonModule, DxDataGridModule, DxPopupModule, DxTreeListModule} from 'devextreme-angular';
import {NbChoicesModule} from 'nb-choices';

const routes: Routes = [
  {
    path: 'list',
    component: JamiaListGcComponent,
    data: {
      title: 'Org List',
    }
  },
  {
    path: 'update/:id',
    component: JamiaUpdateComponent,
    data: {
      title: 'Jamia Update',
    },
  }
];
@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    AlertModule.forRoot(),
    TabsModule,
    MatIconModule,
    MatTabsModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    DxTreeListModule,
    NbChoicesModule,
    DxDataGridModule,
    DxButtonModule,
    DxPopupModule,
  ],
  declarations: [
    JamiaUpdateComponent,
    JamiaListGcComponent,
  ],
})
export class JamiaModule {}
