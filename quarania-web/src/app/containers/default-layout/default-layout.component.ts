import {Component, OnDestroy, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {NavData, navItems} from '../../_nav';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {JwtHelperService} from '@auth0/angular-jwt';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  private authService: AuthService;
  itemsArray: NavData[] = [];

  constructor(@Inject(DOCUMENT) _document?: any, authService?: AuthService, private route?: Router) {
    this.authService = authService;
    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    // localStorage.getItem("role");
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  filterNavItems(items: NavData[]) {
    // this.itemsArray = [];
    const role = this.authService.getRole();
    items.forEach(item => {
      const i: NavData = JSON.parse(JSON.stringify(item));
      this.filterItem(role, this.itemsArray, i, item);
      if (item.children) {
        i.children = [];
        item.children.forEach((c: NavData) => {
          this.filterNavItemsChildren(c, i);
        });
      }
    });
    return this.itemsArray;
  }

  private filterItem(role, itemsArray: any[], i, item: NavData) {
    if (role === 'super') {
      itemsArray.push(i);
      return;
    }
    if (item.role !== null && item.role !== 'super' && role === 'admin') {
      itemsArray.push(i);
    }
    if (item.role !== null && item.role === 'user' && role === 'user') {
      itemsArray.push(i);
    }
  }

  filterNavItemsChildren(item: NavData, parent: NavData) {
    const role = this.authService.getRole();
    const itemsArray = [];
    const i = JSON.parse(JSON.stringify(item));
    if (item.children) {
      i.children = [];
    }
    if (role === 'super') {
      parent.children.push(i);
    }
    if (item.role !== null && item.role !== 'super' && role === 'admin') {
      parent.children.push(i);
    }
    if (item.role !== null && item.role === 'user' && role === 'user') {
      parent.children.push(i);
    }
    if (item.children) {
      item.children.forEach(c => {
        this.filterNavItemsChildren(c, i);
      });
    }

  }

  navItems() {
    return this.filterNavItems(navItems);
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  onLogout() {
    this.authService.removeToken();
    this.route.navigate(['/login']);
  }
}
