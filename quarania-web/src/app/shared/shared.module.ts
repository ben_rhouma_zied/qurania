import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingDropdownComponent } from './setting-dropdown/setting-dropdown.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

@NgModule({
  imports: [CommonModule, BsDropdownModule],
  declarations: [SettingDropdownComponent],
  exports: [SettingDropdownComponent],
})
export class SharedModule {}
