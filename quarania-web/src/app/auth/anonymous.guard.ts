import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


export class AnonymousGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthService) { }
    canActivate(): boolean {
        if (!this.authService.loggedIn()) {
            return true;
        }
        this.router.navigate(['/dashboard']);
        return false;
    }
}
