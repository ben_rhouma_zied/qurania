import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


export class AuthGuard implements CanActivate {

    constructor(private router: Router, private authService: AuthService) { }
    canActivate(): boolean {
        // return true;
        if (!this.authService.loggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }
}
