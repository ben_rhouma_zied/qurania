import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {

  constructor(private router: Router, private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const accessToken = this.authService.getToken();
    console.log(`intercepting http request ${accessToken}`);
    if (!!accessToken) {
      const cloned = request.clone({
        setHeaders: {
          Authorization: `Bearer ${accessToken}`
        }
      });
      const handled = next.handle(cloned);
      handled.pipe(tap(
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            this.catchErrors(err);
          }
        }
      ));
      return handled;
    }
    return next.handle(request);
  }

  private catchErrors(res: HttpErrorResponse) {
    console.log(res);
    if (this.isError(res)) {
      console.log(`Internal server error occurred (${res.status} - ${res.statusText})`);
      this.router.navigateByUrl('/error');
    } else if (this.isUnauthorized(res)) {
      console.log(`User is not authenticated  - not logged in or the session expired? (${res.status} - ${res.statusText})`);
      this.router.navigateByUrl('/logout');
    } else if (this.isForbidden(res)) {
      console.log(`User does not have necessary permissions for the resource (${res.status} - ${res.statusText}): ${res.url}`);
      this.router.navigateByUrl('/forbidden');
    }
  }

  isError(res: HttpErrorResponse): boolean {
    return res && res.status === 500;
  }

  isUnauthorized(res: HttpErrorResponse): boolean {
    return res && res.status === 401;
  }

  isForbidden(res: HttpErrorResponse): boolean {
    return res && res.status === 403;
  }

}
