import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {JwtHelperService} from '@auth0/angular-jwt';
@Injectable({providedIn: 'root'})
export class AuthService {
    constructor(private httpClient: HttpClient, private cookieService: CookieService) {}
    JWToken = 'JW_token';
    loginUser(email: string, password: string): Observable<any> {
        try {
            const payload = {
                email,
                password
            };
            return this.httpClient.post(`${
                environment.backendHost
            }/auth/login`, payload);
        } catch (err) {
            throw err;
        }
    }

    loggedIn() {
        return !!this.cookieService.get(this.JWToken);
    }

    getToken() {
        return this.cookieService.get(this.JWToken);
    }

    getRole() {
      const token = this.getToken();
      const helper = new JwtHelperService();
      return helper.decodeToken(token).user_type;
    }


    getLoggedUserId(): string {
      const token = this.getToken();
      const helper = new JwtHelperService();
      return helper.decodeToken(token).id;
    }

    setToken(token: string) {
        return this.cookieService.set(this.JWToken, token);
    }

    removeToken() {
        return this.cookieService.delete(this.JWToken);
    }

}
