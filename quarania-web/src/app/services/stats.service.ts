import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ProjectQuery } from 'app/models/project.model';

export class StatsService {
  constructor(private httpClient: HttpClient) {}

  async getTotalProjects() {
    let total = 0;
    const url = `${environment.baseApiUrl}/projects/1/1`;
    const result: any = await this.httpClient.get(url).toPromise();
    if (result.statusCode === 200) {
      total = result.data.totalDocs;
    }
    return total;
  }

  async getTotalUserByRole(role) {
    let total = 0;
    const url = `${environment.baseApiUrl}/users/query`;
    const params: HttpParams = new HttpParams()
      .set('role', role)
      .set('page', '1')
      .set('limit', '0');
    const result: any = await this.httpClient.get(url, { params }).toPromise();
    if (result.statusCode === 1) {
      total = result.data.totalDocs;
    }
    return total;
  }
}
