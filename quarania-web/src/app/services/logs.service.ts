import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class LogService {
    constructor(private httpClient: HttpClient) { }
    getLogs(page: number, limit: number): Observable<any> {
      return this.httpClient.get(`${environment.baseApiUrl}/logs/${page}/${limit}`);
    }
}
