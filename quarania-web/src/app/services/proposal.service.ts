import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ProposalModel } from '../models/proposal.model';
@Injectable({
  providedIn: 'root',
})
export class ProposalService {
  private BASE_URL = `${environment.baseApiUrl}/bids/proposals`;
  constructor(private http: HttpClient) {}

  getSCProposals(page: number, limit: number, scId: string) {
    let params: HttpParams = new HttpParams();
    params = params.set('page', page.toString());
    params = params.set('limit', limit.toString());
    params = params.set('scId', scId);
    return this.http.get(`${this.BASE_URL}/submitted`, { params });
  }
  getProposalsByBid(page: number, limit: number, bidId: string) {
    let params: HttpParams = new HttpParams();
    params = params.set('page', page.toString());
    params = params.set('limit', limit.toString());
    params = params.set('bidId', bidId);
    return this.http.get(`${this.BASE_URL}`, { params });
  }

  getProposal(proposalId: string) {
    let params: HttpParams = new HttpParams();
    params = params.set('proposalId', proposalId);
    return this.http.get(`${this.BASE_URL}/by/id`, { params });
  }

  createProposal(proposal: ProposalModel) {
    return this.http.post(`${this.BASE_URL}/create`, proposal);
  }

  acceptProposal(proposalId: string) {
    const payload = {
      proposalId,
    };
    return this.http.put(`${this.BASE_URL}/accept`, payload);
  }

  refuseProposal(proposalId: string) {
    const payload = {
      proposalId,
    };
    return this.http.put(`${this.BASE_URL}/refuse`, payload);
  }

  getProposalBidSc(bidId: string, scId: string) {
    const payload = {
      bidId,
      scId,
    };
    return this.http.post(`${this.BASE_URL}/get`, payload);
  }
}
