import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { ProjectQuery } from 'app/models/project.model';

export class ProjectService {
    constructor(private httpClient: HttpClient) { }

    async getProjects(projectQuery: ProjectQuery) {
        const url = `${environment.baseApiUrl}/projects/${projectQuery.page}/${projectQuery.limit}`;
        let params: HttpParams = new HttpParams();
        if (projectQuery.query && projectQuery.queryField) {
            params = params.set('query', projectQuery.query);
            params = params.set('queryField', projectQuery.queryField);
        }
        if (projectQuery.sort && projectQuery.sortField) {
            params = params.append('sort', projectQuery.sort);
            params = params.append('sortField', projectQuery.sortField);
        }
        return this.httpClient.get(url, { params }).toPromise();
    }

    async getProject(projectID: string) {
        const url = `${environment.baseApiUrl}/projects/get/${projectID}`;
        return this.httpClient.get(url).toPromise();
    }

}
