import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  BASE_URL = `${environment.baseApiUrl}/payment`;
  BASE_URL_SUB = `${environment.baseApiUrl}/subscription`;
  constructor(private httpClient: HttpClient) {}

  createCustomer(userID, email, userRole, tokenID): Observable<any> {
    const payload = {
      userID,
      email,
      userRole,
      tokenID,
    };
    return this.httpClient.post(`${this.BASE_URL}/createCustomer`, payload);
  }

  addCard(customerID, tokenID) {
    const payload = {
      customerID,
      tokenID,
    };
    return this.httpClient.post(`${this.BASE_URL}/addCard`, payload);
  }

  setDefaultCard(customerID, tokenID) {
    const payload = {
      customerID,
      tokenID,
    };
    return this.httpClient.post(`${this.BASE_URL}/defaultCard`, payload);
  }

  deleteCard(customerID, tokenID) {
    const payload = {
      customerID,
      tokenID,
    };
    return this.httpClient.post(`${this.BASE_URL}/deleteCard`, payload);
  }
  getCustomer(customerID: string): Observable<any> {
    const payload = { customerID };
    return this.httpClient.post(`${this.BASE_URL}/getCustomer`, payload);
  }

  getCustomers(limit: number | undefined): Observable<any> {
    const payload = { limit };
    return this.httpClient.post(`${this.BASE_URL}/getCustomers`, payload);
  }

  createSubscription(customerID: string, planID: string): Observable<any> {
    const payload = { customerID, planID };
    return this.httpClient.post(`${this.BASE_URL}/createSubscription`, payload);
  }

  getSubscriptions(limit: number | undefined): Observable<any> {
    const payload = { limit };
    return this.httpClient.post(`${this.BASE_URL}/getSubscriptions`, payload);
  }

  getStripeSubscription(subID): Observable<any> {
    const payload = { subID };
    return this.httpClient.post(`${this.BASE_URL}/getSubscription`, payload);
  }

  getPlans(): Observable<any> {
    return this.httpClient.get(`${this.BASE_URL}/plans`);
  }

  getCharges(limit: number | undefined): Observable<any> {
    const payload = { limit };
    return this.httpClient.post(`${this.BASE_URL}/getCharges`, payload);
  }

  // Subs

  getAllSubscriptions(limit: number, page: number): Observable<any> {
    const payload = { limit, page };
    return this.httpClient.post(`${this.BASE_URL_SUB}/all`, payload);
  }
  getSubscriptionById(subID): Observable<any> {
    const payload = { subID };
    return this.httpClient.post(`${this.BASE_URL_SUB}/get`, payload);
  }
  getAllActivities(limit: number, page: number): Observable<any> {
    const payload = { limit, page };
    return this.httpClient.post(`${this.BASE_URL_SUB}/allActivities`, payload);
  }
  getActivitiesBySubscriptions(limit: number, page: number): Observable<any> {
    const payload = { limit, page };
    return this.httpClient.post(`${this.BASE_URL_SUB}/all`, payload);
  }
  getActivitiesByUser(limit, page, userID): Observable<any> {
    const payload = { limit, page, userID };
    return this.httpClient.post(`${this.BASE_URL_SUB}/getActivitiesByUser`, payload);
  }
}
