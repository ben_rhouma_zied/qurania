import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { TableQuery } from '../models/table.model';
import {Order} from '../models/Order';



@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private httpClient: HttpClient) {
  }


  async getEntities() {
    return await this.httpClient.get(`${environment.baseApiUrl}/organisations`).toPromise();
  }

  async getUsers(id, type) {
    return this.httpClient.get(`${environment.baseApiUrl}/users?type=${type}&id=${id}` ).toPromise();
  }

  getUser(id: string): Observable<any> {
    const url = `${environment.baseApiUrl}/users/${id}`;
    return this.httpClient.get(url);
  }

  getUserToken(userID: string) {
    const body = {userID};
    return this.httpClient.post(`${environment.baseApiUrl}/users/loginasuser`, body);
  }

  changePassword(id, password) {
    const body = {password};
    return this.httpClient.post(`${environment.baseApiUrl}/users/${id}/change/password`, body);
  }

  createUser(user: UserModel, password, confirmPassword) {
    const body = {
      password: password,
      passwordConfirm: confirmPassword,
      firstName: user.first_name,
      lastName: user.last_name,
      email: user.email,
      choice: user.entity
    };
    return this.httpClient.post(`${environment.baseApiUrl}/users/new`, body);
  }

  updateUser(user: UserModel) {
    const body = {
      _id: user._id,
      firstName: user.first_name,
      lastName: user.last_name,
      email: user.email,
      entity: user.entity,
      type: user.type
    };
    return this.httpClient.put(`${environment.baseApiUrl}/users/${user._id}`, body);
  }

  async deleteUser(id: string) {
    console.log(`deleting user ${id}`);
    return this.httpClient.put(`${environment.baseApiUrl}/user/delete/${id}`, {}).toPromise();
  }

  async uploadFile(formData) {
    return this.httpClient.post(`${environment.baseApiUrl}/users/upload`, formData, {
      reportProgress: true,
      observe: 'events'
    }).toPromise();
      // .subscribe(events => {
      //   if (events.type === HttpEventType.UploadProgress) {
      //     console.log('Upload progress: ', Math.round(events.loaded / events.total * 100) + '%');
      //   } else if (events.type === HttpEventType.Response) {
      //     console.log(events);
      //   }
      // });
  }
}
