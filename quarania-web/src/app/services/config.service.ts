import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  get config() {
    return this._config;
  }

  set config(value) {
    this._config = value;
  }
  constructor(private httpClient: HttpClient) {
  }

  private _config;

  getConfig(id?) {
    if (id) {
      return this.httpClient.get(`${environment.baseApiUrl}/configuration/id/` + id, {}).toPromise();
    } else {
      return this.httpClient.get(`${environment.baseApiUrl}/configuration/default`, {}).toPromise();
    }
  }

  getConfigs() {
    return this.httpClient.get(`${environment.baseApiUrl}/configuration`, {}).toPromise();
  }

  saveConfig(config) {
    return this.httpClient.post(`${environment.baseApiUrl}/configuration`, JSON.stringify(config)).toPromise();
  }

  deleteConfig(id) {
    return this.httpClient.delete(`${environment.baseApiUrl}/configuration/` + id).toPromise();
  }


}
