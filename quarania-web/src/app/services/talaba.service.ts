import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { TalabaModel } from '../models/talaba.model';
import { TableQuery } from '../models/table.model';

@Injectable({
  providedIn: 'root',
})
export class TalabaService {
  constructor(private httpClient: HttpClient) {
  }

  getTalabas(talabasQuery: TableQuery): Observable<any> {
    let params: HttpParams = new HttpParams()
      .set('page', `${talabasQuery.page}`)
      .set('limit', `${talabasQuery.limit}`);
    if (talabasQuery.query && talabasQuery.queryField) {
      params = params.set('query', talabasQuery.query);
      params = params.set('queryField', talabasQuery.queryField);
    }
    if (talabasQuery.sort.length > 0 && talabasQuery.sortField) {
      params = params.append('sort', talabasQuery.sort);
      params = params.append('sortField', talabasQuery.sortField);
    }
    console.log(`${environment.baseApiUrl}`);
    return this.httpClient.get(`${environment.baseApiUrl}/talabas`, {params});
  }

  getTalaba(id: string): Observable<any> {
    const url = `${environment.baseApiUrl}/talabas/${id}`;
    return this.httpClient.get(url);
  }

  getTalabaToken(talabaID: string) {
    const body = {talabaID};
    return this.httpClient.post(`${environment.baseApiUrl}/talabas/loginastalaba`, body);
  }

  changePassword(talabaID, password) {
    const body = {talabaID, password};
    return this.httpClient.post(`${environment.baseApiUrl}/talabas/changetalabapass`, body);
  }

  createTalaba(talaba: TalabaModel) {
    const body = {
      firstName: talaba.first_name,
      lastName: talaba.last_name,
      email: talaba.email,
      age : talaba.age,
    };
    return this.httpClient.post(`${environment.baseApiUrl}/taleb/create`, body);
  }

  updateTalaba(talaba: TalabaModel) {
    const body = {
      talabaID: talaba._id,
      firstName: talaba.first_name,
      lastName: talaba.last_name,
    };
    return this.httpClient.put(`${environment.baseApiUrl}/talabas`, body);
  }

  deleteTalaba(talaba: TalabaModel) {
    return this.httpClient.get(`${environment.baseApiUrl}/talaba/delete/${talaba._id}`);
  }

}
