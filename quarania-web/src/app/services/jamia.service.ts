import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { JamiaModel } from '../models/jamia.model';
import { TableQuery } from '../models/table.model';
@Injectable({
  providedIn: 'root',
})
export class JamiaService {
  constructor(private httpClient: HttpClient) {}

  getJamias() {
    return this.httpClient.get(`${environment.baseApiUrl}/organisations`, {}).toPromise();
  }

  getJamia(id: string): Observable<any> {
    const url = `${environment.baseApiUrl}/organisations/${id}`;
    return this.httpClient.get(url);
  }


  updateJamia(id, users) {
    const body = {
      id: id,
      users : users
    };
    return this.httpClient.post(`${environment.baseApiUrl}/organisations/` + id, body);
  }
}
