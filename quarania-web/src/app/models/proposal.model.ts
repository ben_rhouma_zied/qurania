export interface ProposalModel {
  amount: number;
  period: number;
  attachment_url: string;
  _id?: string;
  bidId: any;
  signature: any;
  scId: any;
  gcId: any;
  type: string;
  status?: string;
  createdAt?: Date;
  updatedAt?: Date;
  questions?: Array<ResponseModel>;
}

export interface TakeoffModel {
  _id?: string;
  source: string;
  image: string;
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: any;
  bid: any;
}

export interface ResponseModel {
  question: string;
  response: string;
  type: string;
}
