import { UserModel } from './user.model';

export interface Pagination<Model> {
  docs: Model[];
  totalDocs: number;
  limit: number;
  hasPrevPage: boolean;
  hasNextPage: boolean;
  page: number;
  totalPages: number;
  pagingCounter: number;
  prevPage: number;
  nextPage: number;
}
