export interface UserModel extends BaseModel {
  first_name: string;
  last_name: string;
  email: string;
  image?: string;
  entity?: Array<string>;
  results?: Array<string>;
  password: string;
  type: String;
}
