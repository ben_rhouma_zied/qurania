export interface Alert {
    type: string;
    title: string;
    msg: string;
    date: Date;
}
