
export interface TableQuery {
  page: number;
  limit: number;
  query?: string;
  queryField?: string;
  sortField?: string;
  sort?: string;
}
