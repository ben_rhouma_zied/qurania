export interface LogModel extends BaseModel {
  action: string;
  comment: string;
  type: string;
  data: string;
  section: string;
  id: string;
}
