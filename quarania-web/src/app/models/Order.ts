export class Order {
  ID: number;
  OrderNumber: number;
  OrderDate: string;
  SaleAmount: number;
  Terms: string;
  CustomerInfo: {
    StoreState: string;
    StoreCity: string;
  };
  Employee: string;
}
