
export interface ChatRoomModel extends BaseModel {
    type: string;
    roomID: string;
    user1: {
        _id: string;
        name: string;
        role: string;
    };
    user2: {
        _id: string;
        name: string;
        role: string;
    };
}

export interface MessageModel extends BaseModel {
    content: string;
    userID: string;
    seen: boolean;
    seenAt: Date;
}

