import { UserModel } from './user.model';

export interface ProjectModel extends BaseModel {
  createdBy: string;
  updatedBy: string;
  projectName: string;
  projectDesc: string;
  location: string;
  timeZone: {
    label: string;
    name: string;
    value: string;
  };
  bidsLength: number;
  isPending: string;
  projImg: string;
  openBidsLength: string;
  awardedBidsLength: string;
  projectId: number;
  projectStartTime: Date;
  projectEndTime: Date;
  owner?: UserModel;
}

export interface ProjectQuery {
  page: number;
  limit: number;
  query?: string;
  queryField?: string;
  sortField?: string;
  sort?: string;
}
