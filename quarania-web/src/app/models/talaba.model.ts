export interface TalabaModel extends BaseModel {
  first_name: string;
  last_name: string;
  email: string;
  image?: string;
  age?: number;
}
