'use strict';

const mongoose = require('mongoose');
const Promise = require('bluebird');
const validator = require('validator');
const UserModel = require('../model/User.js');

// mongoose.Promise = Promise;

const Schema = mongoose.Schema;

const mongoosePaginatePlugin = require('mongoose-paginate-v2');


const ConfigSchema = new Schema(
	{
		ConfigType: {
			type: String,
			enum: ['percentage', 'total']
		},
		name : String,
		amount: Number, // if to total ==> amount could be 100 , 80 , 20 ..
		// if percentage amount is not computed 100% will be applied
		options: [ // 2 blocks
			{
				_id: { type: Schema.ObjectId, auto: true },
				name: String, // ta9yim 7ifdh | ta9yim tilawa
				global: Boolean, // calculate the bloc after exam
				actif: Boolean, // calculate this or not
				amount: Number, // how much points to assign to bloc
				options: [
					{
						_id: { type: Schema.ObjectId, auto: true },
						actif: Boolean,
						name: String,
						options: [
							{
								_id: { type: Schema.ObjectId, auto: true },
								name: String,
								value: Number,
								global: Boolean,
							}
						]

					}
				]
			}
		]
	});


ConfigSchema.plugin(mongoosePaginatePlugin)
const model = mongoose.model('Configuration', ConfigSchema);




//
const mongoString = 'mongodb://localhost/qurania';



const createErrorResponse = (statusCode, message) => ({
    statusCode: statusCode || 501,
    headers: { 'Content-Type': 'text/plain' },
    body: message || 'Incorrect id',
});

const dbExecute = (db, fn) => db.then(fn)
	//.finally(() => db.close());

function dbConnectAndExecute(dbUrl, fn) {
    return dbExecute(mongoose.connect(dbUrl), fn);
}

function callback(x,y){
    console.log(y);
}

dbConnectAndExecute(mongoString, () => (
	model
        .paginate({})
         .then(user => callback(null, { statusCode: 200, body: JSON.stringify(user) }))
         .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
));


/**

 [{
 	"ConfigType": "percentage",
 	"amount": 100,
 	"options": [{
 		"name": "ta9yim 7ifdh",
 		"global": false,
 		"actif": true,
 		"amount": 100,
 		"options": [{
 				"actif": true,
 				"name": "Fat7",
 				"options": [{
 					"name": "default",
 					"value": 2,
 					"global": false
 				}]

 			},
 			{
 				"actif": true,
 				"name": "tanbih",
 				"options": [{
 					"name": "default",
 					"value": 1,
 					"global": false
 				}]

 			}
 		]
 	}]
 }]
 */
