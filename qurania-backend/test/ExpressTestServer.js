const express = require('express');
const app = express();
app.use(express.json())
const port = 3000;


const mongoose = require('mongoose');
const Promise = require('bluebird');
const ConfigModel = require('../model/configuration');

mongoose.Promise = Promise;

//
const mongoString = 'mongodb://localhost/qurania';



const createErrorResponse = (statusCode, message) => ({
    statusCode: statusCode || 501,
    headers: { 'Content-Type': 'text/plain' },
    body: message || 'Incorrect id',
});

const dbExecute = (db, fn) => db.then(fn).finally(() => db.close());

function dbConnectAndExecute(dbUrl, fn) {
    return dbExecute(mongoose.connect(dbUrl, { useMongoClient: true }), fn);
}


app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
});

const callback = (statusCode, message) => ({
    statusCode: statusCode || 501,
    headers: { 'Content-Type': 'text/plain' },
    body: message || 'Incorrect id',
});


app.post('/config', (request, response) => {
    const conf = new ConfigModel(...request.body);

    if (conf.validateSync()) {
        callback(null, createErrorResponse(400, 'Incorrect user data'));
        return;
    }

    dbConnectAndExecute(mongoString, () => (
        conf
            .save()
            .then(() => callback(null, {
                statusCode: 200,
                body: JSON.stringify({ id: conf.id }),
            }))
            .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
    ));

    response.send('Hello from Express!')
});


app.get('/', (request, response) => {
    response.send('Hello from Express!')
});




