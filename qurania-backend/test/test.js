// import async to make control flow simplier
'use strict';

const async = require('async');

// import the rest of the normal stuff
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create an export function to encapsulate the model creation
const ConfigSchema = new Schema(
    {
        ConfigType: {
            type: String,
            enum: ['percentage', 'total']
        },
        amount: Number, // if to total ==> amount could be 100 , 80 , 20 ..
        // if percentage amount is not computed 100% will be applied
        options: [ // 2 blocks
            {
                name: String, // ta9yim 7ifdh | ta9yim tilawa
                global: Boolean, // calculate the bloc after exam
                actif: Boolean, // calculate this or not
                amount: Number, // how much points to assign to bloc
                options: [
                    {
                        actif: Boolean,
                        name: String,
                        options: [
                            {
                                name: String,
                                value: Number,
                                global: Boolean,
                            }
                        ]

                    }
                ]
            }
        ]
    });

mongoose.model('configuration', ConfigSchema);


const config = mongoose.model('configuration');

// define some dummy data
const data = [
    {
        "ConfigType": "percentage", //
        "amount": 100,
        "options": [{
            "name": "ta9yim 7ifdh",
            "global": false, // calculate the bloc after exam
            "actif": true, // calculate this or not
            "amount": 100, // how much points to assign to bloc
            "options": [
                {
                    "actif": true,
                    "name": "Fat7",
                    "options": [
                        {
                            "name": "default",
                            "value": 2,
                            "global": false
                        }
                    ]

                },
                {
                    "actif": true,
                    "name": "tanbih",
                    "options": [
                        {
                            "name": "default",
                            "value": 1,
                            "global": false
                        }
                    ]

                }
            ]
        }]
    }
];

// const mongoString = 'mongodb://qurania:Ferrari-8558@qurania-shard-00-00-wy07y.mongodb.net:27017,qurania-shard-00-01-wy07y.mongodb.net:27017,qurania-shard-00-02-wy07y.mongodb.net:27017/qurania?ssl=true&replicaSet=qurania-shard-0&authSource=admin&retryWrites=true&w=majority'; // MongoDB Url
mongoose.connect('mongodb://localhost/qurania', function (err) {
    if (err)
        throw err;
    // create all of the dummy people
    async.each(data, function (item, cb) {
        config.create(item, cb);
    }, function (err) {
        if (err) { // handle error
            console.log(' -------------------------  * * * * *')
        }
        cleanup();
    });
});

function cleanup() {
    mongoose.disconnect();

}
