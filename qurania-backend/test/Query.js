// import async to make control flow simplier
'use strict';
const mongoose = require('mongoose');
const async = require('async');
const Organisation = require("../model/Organisation");
const User = require("../model/user");
// define some dummy data
const data = [
    // {"_id": "TN-11", "name": "Tunis", "parent": null},
    // {"_id": "TN-12", "name": "Ariana", "parent": null},
    // {"_id": "TN-13", "name": "Ben Arous", "parent": null},
    // {"_id": "TN-14", "name": "La Manouba", "parent": null},
    // {"_id": "TN-21", "name": "Nabeul", "parent": null},
    // {"_id": "TN-22", "name": "Zaghouan", "parent": null},
    // {"_id": "TN-23", "name": "Bizerte", "parent": null},
    // {"_id": "TN-31", "name": "Béja", "parent": null},
    // {"_id": "TN-32", "name": "Jendouba", "parent": null},
    // {"_id": "TN-33", "name": "Le Kef", "parent": null},
    // {"_id": "TN-34", "name": "Siliana", "parent": null},
    // {"_id": "TN-41", "name": "Kairouan", "parent": null},
    // {"_id": "TN-42", "name": "Kasserine", "parent": null},
    // {"_id": "TN-43", "name": "S_idi Bouz_id", "parent": null},
    // {"_id": "TN-51", "name": "Sousse", "parent": null},
    // {"_id": "TN-52", "name": "Monastir", "parent": null},
    // {"_id": "TN-53", "name": "Mahdia", "parent": null},
    // {"_id": "TN-61", "name": "Sfax", "parent": null},
    // {"_id": "TN-71", "name": "Gafsa", "parent": null},
    // {"_id": "TN-72", "name": "Tozeur", "parent": null},
    // {"_id": "TN-73", "name": "Kébili", "parent": null},
    // {"_id": "TN-81", "name": "Gabès", "parent": null},
    // {"_id": "TN-82", "name": "Médenine", "parent": null},
    // {"_id": "TN-83", "name": "Tataouine", "parent": null},
    // {"_id": "TN-21-01", "name": "Beni Khalled", "parent": "TN-21"},
    // {"_id": "TN-21-02", "name": "Korba", "parent": "TN-21"}
];


const mongoString = 'mongodb://qurania:Ferrari-8558@qurania-shard-00-00-wy07y.mongodb.net:27017,qurania-shard-00-01-wy07y.mongodb.net:27017,qurania-shard-00-02-wy07y.mongodb.net:27017/qurania?ssl=true&replicaSet=qurania-shard-0&authSource=admin&retryWrites=true&w=majority'; // MongoDB Url
// const mongoString = 'mongodb://localhost/qurania';
mongoose.connect(mongoString, function (err) {
    if (err)
        throw err;
    // create all of the dummy people
    let id = "5e62b1e6f8562b19f5ed875c";
    Organisation.find({users: new mongoose.Types.ObjectId(id)})
        .distinct('_id')
        .then(org => {
            // org.map(e => new mongoose.Types.ObjectId(e))
            console.log(org);

        })

        .catch(err => {
            console.error(err);
        })
        .finally(() => mongoose.disconnect())
    // mongoose.disconnect();

});
