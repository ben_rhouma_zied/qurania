const mongoose = require('mongoose');
const validator = require('validator');

const Schema = mongoose.Schema;

const UserSchema = new Schema(
    {
        _id: {type: Schema.ObjectId, auto: true},
        email: {
            type: String
        },
        password: {
            type: String,
        },
        first_name: {
            type: String,
            required: true,
        },
        last_name: {
            type: String,
            required: true,
        },
        image: String,
        entity: [
            {type: String, ref: 'Organisation'}
        ],
        supervisor: [
            {type: Schema.Types.ObjectId, ref: 'user'}
        ],
        results: [
            {type: Schema.Types.ObjectId, ref: 'result'}
        ],
        type: {type: String, enum: ['user', 'admin', 'super'], default: 'user'}
    });

const mongoosePaginate = require('mongoose-paginate-v2');

UserSchema.plugin(mongoosePaginate);

const model = mongoose.model('user', UserSchema);

module.exports = model;
