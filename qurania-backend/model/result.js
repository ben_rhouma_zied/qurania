const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const configSchema = require ('./schema/config_schema');

let Detail= {
	name : String,
	amount : Number
};

let  Bloc = {
	"name": String,
	minus : [Detail],
	plus : [Detail]
};

const ResultSchema = new Schema( {
	_id: {type: Schema.ObjectId, auto: true},
    amount : Number,
    name : String,
	dateTime : Date,
	userId: {type: String},
	details : [Bloc],
    config : configSchema
});





const model = mongoose.model('result', ResultSchema);

module.exports = model;
