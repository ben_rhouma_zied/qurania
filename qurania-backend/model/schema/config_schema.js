const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigSchema = new Schema(
    {
        ConfigType: {
            type: String,
            enum: ['percentage', 'total']
        },
        name : String,
        amount: Number, // if to total ==> amount could be 100 , 80 , 20 ..
        // if percentage amount is not computed 100% will be applied
        options: [ // 2 blocks
            {
                _id: { type: Schema.ObjectId, auto: true },
                name: String, // ta9yim 7ifdh | ta9yim tilawa
                global: Boolean, // calculate the bloc after exam
                actif: Boolean, // calculate this or not
                amount: Number, // how much points to assign to bloc
                options: [
                    {
                        _id: { type: Schema.ObjectId, auto: true },
                        actif: Boolean,
                        name: String,
                        options: [
                            {
                                _id: { type: Schema.ObjectId, auto: true },
                                name: String,
                                value: Number,
                                global: Boolean,
                            }
                        ]

                    }
                ]
            }
        ]
    });

module.exports =  ConfigSchema;
