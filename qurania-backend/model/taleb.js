const mongoose = require('mongoose');
const validator = require('validator');

const Schema = mongoose.Schema;

const TalebSchema = new Schema(
    {
        _id: {type: Schema.ObjectId, auto: true},
      first_name: {
        type: String,
        required: true,
      },
      last_name: {
        type: String,
        required: true,
      },
      email: {
        type: String,
        required: true,
      },
      age : Number,
      image: String
    });

const mongoosePaginate = require('mongoose-paginate-v2');
TalebSchema.plugin(mongoosePaginate);
const model = mongoose.model('subject', TalebSchema);

module.exports = model;
