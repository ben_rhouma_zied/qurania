const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const OrganisationSchema = new Schema(
    {
        _id: {type: String},
        name: {
            type: String,
            required: true,
        },
        parent: String,
        users: [{ type: Schema.Types.ObjectId, ref: 'user' }],
    });

const mongoosePaginate = require('mongoose-paginate-v2');
OrganisationSchema.plugin(mongoosePaginate);
const model = mongoose.model('Organisation', OrganisationSchema);

module.exports = model;
