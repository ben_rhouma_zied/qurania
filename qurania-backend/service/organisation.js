const createErrorResponse = require('./common').createErrorResponse;
const dbConnectAndExecute = require('./common').dbConnectAndExecute;
const Organisation = require('../model/Organisation');
const Users = require('../model/user');
const validator = require('validator');

module.exports.organisations = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    dbConnectAndExecute((mongoose) => {
            Organisation
                .find()
                .populate('users')
                .then(result => callback(null, {statusCode: 200, headers: {
                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                    }, body: JSON.stringify(result)}))
                .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

        }
    );
};

module.exports.get_organisation_by_id = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false

    dbConnectAndExecute((mongoose) => {

            Organisation.findOne({_id: event.pathParameters.id})
                .populate('users')
                .then(
                    org => {
                        Users
                            .find({type: {$ne: "user"}})
                            .then(users => callback(null, {
                                statusCode: 200,    headers: {
                                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                                },
                                body: JSON.stringify({organisation: org, users: users})
                            }))
                            .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

                    }
                )
                .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
        }
    );
};

module.exports.assign_org_supervisor = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    let body = JSON.parse(event.body);
    dbConnectAndExecute((mongoose) => {
            let update = {
                $set: {users: body.users}
            };
            Organisation
                .updateOne({_id: event.pathParameters.id}, update)
                .then(callback(null, {statusCode: 200,    headers: {
                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                    }, body: ""}))
                .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

        }
    );
};
