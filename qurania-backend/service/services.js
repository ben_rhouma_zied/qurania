const createErrorResponse = require('./common').createErrorResponse;
const dbConnectAndExecute = require('./common').dbConnectAndExecute;
const validator = require('validator');

const ResultModel = require('../model/result');

module.exports.saveResult = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    const conf = new ResultModel(JSON.parse(event.body));
    conf.dateTime = new Date();

    if (conf.validateSync()) {
        console.error(conf.errors);
        callback(null, createErrorResponse(501, 'Incorrect configuration data '));
        return;
    }

    dbConnectAndExecute((mongoose) => (
        conf
            .save()
            .then(() => callback(null, {
                statusCode: 200,    headers: {
                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                },
                body: JSON.stringify({id: conf.id}),
            }))
            .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

    ));
};


module.exports.results = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    if (!validator.isAlphanumeric(event.pathParameters.id)) {
        callback(null, createErrorResponse(400, 'Incorrect id'));
        return;
    }

    dbConnectAndExecute((mongoose) => (
        ResultModel
            .find({userId: event.pathParameters.id})
            .then(result => callback(null, {statusCode: 200,    headers: {
                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                }, body: JSON.stringify(result)}))
            .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

    ));
};





