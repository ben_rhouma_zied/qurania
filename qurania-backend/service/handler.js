const ConfigModel = require("../model/configuration");
const ResultModel = require("../model/result");
const jwtKey = require('./common').jwtKey;
const createErrorResponse = require("./common").createErrorResponse;
const dbConnectAndExecute = require("./common").dbConnectAndExecute;
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')

module.exports.configuration = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    if (!event.headers.Authorization) {
        return callback(null, createErrorResponse(401, "Authorization key not found"))
    }
    let tokenString = event.headers.Authorization.replace("Bearer ", "");
    try {
        let payload = jwt.verify(tokenString, jwtKey);
        // let mode = payload.user_type;
        const id = new mongoose.Types.ObjectId(payload.id)
        dbConnectAndExecute((mongoose) =>
            ConfigModel.find({$or: [{userId: null}, {userId: id}]})
                .then(conf =>
                    callback(null, {statusCode: 200, headers: {
                            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                            'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                        }, body: JSON.stringify(conf)})
                )
                .catch(err =>
                    callback(null, createErrorResponse(err.statusCode, err.message))
                )
        );
    } catch (e) {
        return callback(null, createErrorResponse(401, "Authorization key not found"))
    }
};
module.exports.get_configuration_by_id = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    dbConnectAndExecute((mongoose) =>
        ConfigModel.findOne({_id: event.pathParameters.id})
            .then(user =>
                callback(null, {statusCode: 200,headers: {
                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                    }, body: JSON.stringify(user)})
            )
            .catch(err =>
                callback(null, createErrorResponse(err.statusCode, err.message))
            )
    );
};


module.exports.delete_configuration_by_id = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    dbConnectAndExecute(() =>
        ConfigModel.remove({_id: event.pathParameters.id})
            .then(user =>
                callback(null, {statusCode: 200,headers: {
                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                    }, body: JSON.stringify("config deleted")})
            )
            .catch(err =>
                callback(null, createErrorResponse(err.statusCode, err.message))
            )
    );
};

module.exports.createConfig = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    const data = JSON.parse(event.body);
    if (!event.headers.Authorization) {
        return callback(null, createErrorResponse(401, "Authorization key not found"))
    }
    let tokenString = event.headers.Authorization.replace("Bearer ", "");
    try {
        let payload = jwt.verify(tokenString, jwtKey);
        let id = data._id;
        if (!id) {
            id = new mongoose.Types.ObjectId();
        }
        data.userId = new mongoose.Types.ObjectId(payload.id)

        dbConnectAndExecute((mongoose) => {
                ConfigModel.update({_id: id}, data, {upsert: true, setDefaultsOnInsert: true})
                    .then(e =>
                        callback(null, {
                            statusCode: 200,    headers: {
                                'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                            },
                            body: JSON.stringify({id: e._id})
                        })
                    )
                    .catch(err =>
                        callback(null, createErrorResponse(err.statusCode, err.message))
                    );

            }
        );
    } catch (e) {
        return callback(null, createErrorResponse(401, "Authorization key not found"))
    }
};

module.exports.saveResult = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    const conf = new ResultModel(JSON.parse(event.body));
    conf.dateTime = new Date();

    if (conf.validateSync()) {
        console.error(conf.errors);
        callback(null, createErrorResponse(501, "Incorrect configuration data "));
        return;
    }

    dbConnectAndExecute((mongoose) =>
        conf
            .save()
            .then(() =>
                callback(null, {
                    statusCode: 200,    headers: {
                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                    },
                    body: JSON.stringify({id: conf.id})
                })
            )
            .catch(err =>
                callback(null, createErrorResponse(err.statusCode, err.message))
            )
    );
};

module.exports.getDefaultConfiguration = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    dbConnectAndExecute((mongoose) =>
        ConfigModel.findOne({name: "default"})
            .then(user =>
                callback(null, {statusCode: 200,    headers: {
                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                    }, body: JSON.stringify(user)})
            )
            .catch(err =>
                callback(null, createErrorResponse(err.statusCode, err.message))
            )
    );
};
