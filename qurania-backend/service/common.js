const mongoose = require('mongoose');
const Promise = require('bluebird');
const jwtKey = 'my_secret_key';

mongoose.Promise = Promise;

const mongoString = 'mongodb://qurania:Ferrari-8558@qurania-shard-00-00-wy07y.mongodb.net:27017,qurania-shard-00-01-wy07y.mongodb.net:27017,qurania-shard-00-02-wy07y.mongodb.net:27017/qurania?ssl=true&replicaSet=qurania-shard-0&authSource=admin&retryWrites=true&w=majority'; // MongoDB Url


let cachedDb = null;


const createErrorResponse = (statusCode, message) => ({
    statusCode: statusCode || 501,
    headers: {'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
    },
    body: message || 'Incorrect id',
});

const dbExecute = (db, fn) => db
    .then(fn);

function dbConnectAndExecute(fn) {
    if (cachedDb == null) {
        cachedDb = mongoose.connect(mongoString, {
            // Buffering means mongoose will queue up operations if it gets
            // disconnected from MongoDB and send them when it reconnects.
            // With serverless, better to fail fast if not connected.
            bufferCommands: false, // Disable mongoose buffering
            bufferMaxEntries: 0 // and MongoDB driver buffering
        });
    }
    return dbExecute(cachedDb, fn(mongoose));
}


module.exports = {dbConnectAndExecute, createErrorResponse, jwtKey};
