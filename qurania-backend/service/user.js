const createErrorResponse = require('./common').createErrorResponse;
const dbConnectAndExecute = require('./common').dbConnectAndExecute;
const jwtKey = require('./common').jwtKey;
const validator = require('validator');
const jwt = require('jsonwebtoken')
const UserModel = require('../model/user');
const Organisation = require('../model/Organisation');
require('../model/result');
const mongoose = require('mongoose');


const jwtExpirySeconds = 5000;

module.exports.users = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    if (!event.headers.Authorization) {
        return callback(null, createErrorResponse(401, "Authorization key not found"))
    }

    let tokenString = event.headers.Authorization.replace("Bearer ", "");
    try {
        let payload = jwt.verify(tokenString, jwtKey);

        let mode = payload.user_type;
        let id = payload.id;
        if (mode === "super") {
            dbConnectAndExecute((mongoose) => {
                    UserModel
                        .paginate({}, {limit: 5000})
                        // .paginate()
                        .then(result => callback(null, {statusCode: 200,    headers: {
                                'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                            }, body: JSON.stringify(result)}))
                        .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

                }
            );
        }
        if (mode === "admin") {
            dbConnectAndExecute((mongoose) => {
                    Organisation.find({users: new mongoose.Types.ObjectId(payload.id)})
                        .distinct('_id')
                        .then(orgs => {

                            let orgsExp = orgs.map(e => `^${e}.*`).join("|");
                            let filter = [{supervisor: id}];
                            if (orgs.length > 0)
                                filter = [{supervisor: id}, {entity: {$regex: orgsExp}}]
                            UserModel
                                .paginate(
                                    {
                                        $or: filter,
                                        type: {$eq: 'user'},
                                        _id: {$ne: new mongoose.Types.ObjectId(payload.id)},
                                    }
                                    , {limit: 5000})
                                // .paginate()
                                .then(result => callback(null, {statusCode: 200,    headers: {
                                        'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                        'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                                    }, body: JSON.stringify(result)}))
                                .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))
                        })
                        .catch(err => callback(null, createErrorResponse(err.statusCode, err.message)))

                }
            );
        }

        if (mode === "user") {
            dbConnectAndExecute((mongoose) => {
                    UserModel
                        .paginate({_id: payload.id}, {limit: 5000})
                        .then(
                            result => callback(null, {statusCode: 200,    headers: {
                                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                                }, body: JSON.stringify(result)})
                        )
                        .catch(
                            err => callback(null, createErrorResponse(err.statusCode, err.message))
                        )
                }
            );
        }
    } catch (e) {
        return callback(null, createErrorResponse(401, "Authorization key not found"))
    }

};


module.exports.update_password = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    let userBody = JSON.parse(event.body);
    dbConnectAndExecute((mongoose) => {
            UserModel.findOneAndUpdate({_id: event.pathParameters.id}, {password: userBody.password})
                .then(result => callback(null,
                    {
                        statusCode: 200,    headers: {
                            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                            'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                        },
                        body: JSON.stringify(result)
                    }
                    )
                )
                .catch(err => callback(null,
                    createErrorResponse(err.statusCode, err.message)
                    )
                )
        }
    );
};


module.exports.upload_file = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    if (event.requestContext.domainName === "offlineContext_domainName") {
        if (!event.headers.Authorization) {
            return callback(null, createErrorResponse(401, "Authorization key not found"))
        }
        let tokenString = event.headers.Authorization.replace("Bearer ", "");
        try {
            let payload = jwt.verify(tokenString, jwtKey);
            uploadFileContent(event, payload, callback);
        } catch (e) {

        }
    } else {
        console.log("prod mode");
    }

};


function uploadFileContent(event, payload, callback) {
    let parser = require("./parser");
    const mongoose = require('mongoose');
    parser.parseForm(event).then(e => {
        let fs = require('fs');
        let wstream = fs.createWriteStream('file.csv');
        // mode = true ==> attach user to supervisor else supervisor == null
        let supervisorId = e.body.mode ? payload.id : null;

        wstream.write(e.body.file);
        wstream.end();

        let authors = [];
        let csv = require('fast-csv');
        csv.parseString(e.body.file.toString(), {
            headers: true,
            ignoreEmpty: true
        })
            .on("data", function (data) {
                data['_id'] = new mongoose.Types.ObjectId();

                authors.push({
                    _id: new mongoose.Types.ObjectId(),
                    first_name: data.first_name,
                    last_name: data.last_name,
                    email: data.email,
                    password: data.password,
                    image: data.image,
                    entity: [data.entity],
                    supervisor: [new mongoose.Types.ObjectId(supervisorId)],
                    type: 'user'
                });
            })
            .on("end", function () {

                dbConnectAndExecute((mongoose) => {
                        UserModel.create(authors).then(result => callback(null,
                            {
                                statusCode: 200,headers: {
                                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                                },
                                body: JSON.stringify(result)
                            }
                            )
                        )
                            .catch(err => callback(null,
                                createErrorResponse(err.statusCode, err.message)
                                )
                            )
                    }
                );
                // callback(null, {statusCode: 201, body: ""});
            });

    });
}

module.exports.update_user = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    let userBody = JSON.parse(event.body);
    const user =
        {

            login: userBody.email,
            first_name: userBody.firstName,
            last_name: userBody.lastName,
            email: userBody.email,
            entity: userBody.entity,
            type: userBody.type,
            image: userBody.image,
        }
    if (user.type == null)
        delete user.type
    if (user.image == null)
        delete user.image

    dbConnectAndExecute((mongoose) => {
            UserModel.findOneAndUpdate({_id: event.pathParameters.id}, user)
                .then(result => callback(null,
                    {
                        statusCode: 200,    headers: {
                            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                            'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                        },
                        body: JSON.stringify(result)
                    }
                    )
                )
                .catch(err => callback(null,
                    createErrorResponse(err.statusCode, err.message)
                    )
                )
        }
    );
};

module.exports.new_user = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    let userBody = JSON.parse(event.body);
    const user = new UserModel(
        {
            login: userBody.email,
            first_name: userBody.firstName,
            last_name: userBody.lastName,
            password: userBody.password,
            email: userBody.email,
            choice: userBody.choice,
            image: 'http://sg-fs.com/wp-content/uploads/2017/08/user-placeholder.png'
        }
    );
    dbConnectAndExecute((mongoose) => {
            user
                .save()
                .then(() =>
                    callback(null, {
                        statusCode: 200,    headers: {
                            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                            'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                        },
                        body: JSON.stringify({id: user._id})
                    })
                )
                .catch(err =>
                    callback(null, createErrorResponse(err.statusCode, err.message))
                )
        }
    );
};

module.exports.deleteUser = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    dbConnectAndExecute((mongoose) => {
            UserModel.find({_id: event.pathParameters.id})
                .remove()
                .then(() =>
                    callback(null, {
                        statusCode: 200 ,   headers: {
                            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                            'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                        },
                    })
                )
                .catch(err =>
                    callback(null, createErrorResponse(err.statusCode, err.message))
                )
        }
    );
};

module.exports.get_user_by_id = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    if (!validator.isAlphanumeric(event.pathParameters.id)) {
        return callback(null, createErrorResponse(400, "Incorrect id"));
    }

    dbConnectAndExecute((mongoose) =>
            UserModel.find({_id: event.pathParameters.id}).populate('results')
                .then(user =>
                    callback(null, {statusCode: 200,    headers: {
                            'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                            'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                        }, body: JSON.stringify(user)})
                )
                .catch(err =>
                    callback(null, createErrorResponse(err.statusCode, err.message))
                )
        //
    );
};

module.exports.login = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false
    const data = JSON.parse(event.body);
    dbConnectAndExecute((mongoose) => {


        UserModel

            .findOne({email: data.email, password: data.password})
            .populate('entity')
            .then(
                user => {
                    if (user != null) {
                        const token = jwt.sign({
                                id: user._id,
                                login: user.email,
                                user_type: user.type,
                                entity: user.entity,
                            }
                            , jwtKey,
                            {
                                algorithm: 'HS256',
                                expiresIn: jwtExpirySeconds
                            });
                        callback(null,
                            {
                                statusCode: 200,    headers: {
                                    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
                                    'Access-Control-Allow-Credentials': true, // Required for cookies, authorization headers with HTTPS
                                },
                                body: JSON.stringify({"token": token})
                            })
                    } else {
                        callback(null, createErrorResponse(401, "login failed"));
                    }

                }
            )
            .catch(
                err =>
                    callback(null, createErrorResponse(err.statusCode, err.message)))
        //
    });
};





